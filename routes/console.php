<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Rutas de la consola
|--------------------------------------------------------------------------
|
| Este archivo es donde puedes definir todos tus comandos de consola basados en Closure
| comandos de la consola. Cada Closure está ligado a una instancia de comando permitiendo una
| un enfoque simple para interactuar con los métodos IO de cada comando.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');
