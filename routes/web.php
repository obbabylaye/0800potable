<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PotableController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Rutas Web
|--------------------------------------------------------------------------
|
| Aquí es donde puede registrar rutas web para su aplicación. Estas
| rutas son cargadas por el RouteServiceProvider dentro de un grupo que
| contiene el grupo de middleware "web". ¡Ahora crea algo grande!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::post('/Userlogin',[IndexController::class, 'login']);
Route::get('/dashboard',[DashboardController::class, 'index'])->name('home');
Route::get('/profile',[UserController::class, 'profile'])->name('profile');
Route::get('/logout',[UserController::class, 'logout'])->name('logout');
Route::get('/report/new',[PotableController::class, 'new'])->name('reportNew');
Route::get('/report/list',[PotableController::class, 'list'])->name('reportList');
Route::post('/potable/SearchData',[PotableController::class, 'SearchData']);
Route::post('/potable/listarSubcategoria',[PotableController::class, 'listarSubcategoria']);
Route::post('/potable/SearchClient',[PotableController::class, 'SearchClient']);
Route::post('/potable/registerReport',[PotableController::class, 'registerReport']);
Route::get('/report/ShowDataList',[PotableController::class, 'Datalist'])->name('reportDataList');