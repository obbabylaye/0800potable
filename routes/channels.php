<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Canales de transmisión
|--------------------------------------------------------------------------
|
| Aquí puede registrar todos los canales de transmisión de eventos que su
| aplicación soporta. Los callbacks de autorización de los canales dados son
| utilizadas para comprobar si un usuario autentificado puede escuchar el canal.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
