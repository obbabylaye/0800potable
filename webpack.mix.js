const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix proporciona una API limpia y fluida para definir algunos pasos de construcción de Webpack
 | para tu aplicación Laravel. Por defecto, estamos compilando el archivo Sass
 | para la aplicación, así como la agrupación de todos los archivos JS.
 |
 */

mix.js('resources/js/app.js', 'public/js')
	.css('resources/vendor/fontawesome-free/css/all.min.css', 'public/css/')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');

mix.combine(['node_modules/vue-form-wizard/dist/vue-form-wizard.min.css'], 'public/css/component.css');