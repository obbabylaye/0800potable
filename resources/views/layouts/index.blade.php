<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title') | 0800-potable</title>
	<link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
	@yield('content')
</body>
<script src="{{ asset('js/app.js') }}"></script>
</html>