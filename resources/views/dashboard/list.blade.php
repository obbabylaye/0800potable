@extends('layouts.dashboard')
@section('title', "Listado")
@section('main-content')
	<div id="app">
		
		<div class="card-body shadow mb-4 table-responsive" >
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead class="text-center">
					<tr>
						@foreach($table as $key => $value)
							@switch($value->column_name)
							    @case('fecha_crea')
							        <th>Fecha</th>
							        @break
							    @case('cod_reporte')
							        <th>Codigo</th>
							        @break
							    @case('id_us')
							        <th>Usuario</th>
							        @break
							    @case('id_estatu')
							    	<th>Estado</th>
							    	@break
							@endswitch
						@endforeach
						<th>Accion</th>
					</tr>
				</thead>
				<tbody class="text-center">
					@foreach($data as $key3 => $value)
						<tr>
							@foreach($table as $key => $value)
								@foreach($data[$key3] as $key2 => $data_value)
									@if($value->column_name<>'hora_crea' && $value->column_name<>'id_categ' && $value->column_name<>'id_sub_categ' && $value->column_name<>'id_direccion')
										@if($value->column_name == $key2)
											@switch($key2)
											    @case('fecha_crea')
											        <td>{{ $data_value }} {{ $data[$key3]->hora_crea }}</td>
											        @break
											    @case('id_estatu')
											    	<td>
											    		@if($data_value==1)
											    			Activo
											    		@else
											    			Inactivo
											    		@endif
											    	</td>
											    	@break
											    @default
											        <td>{{ $data_value }}</td>
											@endswitch
										@endif
									@endif
								@endforeach
							@endforeach
							<td>
								<div class='btn-group text-center' style='margin:0 auto;'>
		                            <button class='btn btn-primary' value='editar'>
		                                <i class='fas fa-fw fa-edit'></i>
		                            </button>
		                            <button class='btn btn-secondary' value='eliminar'>
		                                <i class='fas fa-fw fa-trash'></i>
		                            </button>
		                        </div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			
		</div>
	</div>
	<script type="text/javascript">
		var table = $('#dataTable').DataTable({
	        language:{
	            "sProcessing":     "Procesando...",
	            "sLengthMenu":     "Mostrar _MENU_ registros",
	            "sZeroRecords":    "No se encontraron resultados",
	            "sEmptyTable":     "Ningún dato disponible en esta tabla",
	            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	            "sInfoPostFix":    "",
	            "sSearch":         "Buscar:",
	            "sUrl":            "",
	            "sInfoThousands":  ",",
	            "sLoadingRecords": "Cargando...",
	            "oPaginate": {
	                "sFirst":    "Primero",
	                "sLast":     "Último",
	                "sNext":     "Siguiente",
	                "sPrevious": "Anterior"
	            },
	            "oAria": {
	                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	            },
	            "buttons": {
	                "copy": "Copiar",
	                "colvis": "Visibilidad"
	            }
	        }
	    });
	</script>
@endsection