@extends('layouts.dashboard')
@section('title', "Registro de reporte")
@section('main-content')
	<div class="card-body wizard-content" id="validation">
        <form action="#" class="validation-wizard wizard-circle" id="reporte">
            <!-- Step 1 -->
            <h6>Categoria del reporte</h6>
            <section >
                <div class="row" id="categorias">
                    <div class="col-6 col-xl-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="" class=" form-control-label">
                                Categoria
                                <span class="text-danger">*</span> 
                            </label>
                            <select name="categoria" id="categoria" class="form-control custom-select required" onchange="search_categ(this)" required>
                                <option value="">Seleccione una opcion</option>
                                @foreach($categoria as $key => $value)
                                	<option value="{{ $value->id_categ }}">{{ $value->nom_categ }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-xl-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="" class="form-control-label">
                                Sub-categoria
                                <span class="text-danger">*</span> 
                            </label>
                            <select name="subCateg" id="subCateg" class="form-control custom-select required" required>
                                <option value="">Seleccione una opcion</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Step 2 -->
            <h6>Registro de reporte</h6>
            <section >
                <div class="row" id="data_reporte">
                    <div class="container">
                        <div class="col-12 col-md-12 col-xl-12 col-sm-12 mt-5 mb-3">
                            <div class="input-group input-group-joined">
                                <input class="form-control" type="text" placeholder="Ingrese el nic o cedula" aria-label="Search" id="nic" name="nic" maxlength="8" minlength="1">
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="button" onclick="search_cliente($('#nic'))">
                                        <i class="fas fa-fw fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12 col-sm-12 mb-3">
                            <label for="">
                                Ubicación del Reporte:
                            </label>
                            <div class="row">
                                <div class="col-4 col-md-4 col-xl-4 col-sm-4 mb-3 form-group">
                                    <select class="custom-select form-control required" id="id_acueducto" name="id_acueducto" onchange="search_municipio(this,$('#id_municipio'))">
                                      <option>Acueducto:</option>
                                      	@foreach($acueducto as $key => $value)
                                      		<option value="{{ $value->id_acueducto }}">{{ $value->nom_acueducto }}</option>
                                      	@endforeach
                                    </select>
                                </div>
                                <div class="col-4 col-md-4 col-xl-4 col-sm-4 mb-3 form-group">
                                    <select class="custom-select form-control required" id="id_municipio" name="id_municipio" onchange="search_parroquia(this,$('#id_parroquia'))">
                                  <option>Municipio:</option>
                                </select>
                                </div>
                                <div class="col-4 col-md-4 col-xl-4 col-sm-4 mb-3 form-group">
                                    <select class="custom-select form-control required" id="id_parroquia" name="id_parroquia">
                                  <option>Parroquia:</option>
                                </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12 col-sm-12 mb-3">
                            <div class="row">
                                <div class="col-6 col-md-6 col-xl-6 col-sm-6 mb-3 form-group">
                                    <input type="text" class="form-control required"  placeholder="Urb/Sector:" id="sector" name="sector">
                                </div>
                                <div class="col-6 col-md-6 col-xl-6 col-sm-6 mb-3 form-group">
                                    <input type="text" class="form-control required"  placeholder="Av/Calle:" id="av" name="av">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12 col-sm-12 mb-3">
                            <div class="row">
                                <div class="col-6 col-md-6 col-xl-6 col-sm-6 mb-3 form-group" >
                                  <label for="exampleFormControlInput1">Nombre de Inmueble:</label>
                                  <input type="text" class="form-control required" id="nom_inmueble" name="nom_inmueble">
                                </div>

                                <div class="col-6 col-md-6 col-xl-6 col-sm-6 mb-3 form-group" >
                                  <label for="exampleFormControlInput1">Punto de Referencia:</label>
                                  <input type="text" class="form-control required" id="punto_referencia" name="punto_referencia">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12 col-sm-12 mb-3">
                            <label for="">
                                Datos del Cliente:
                            </label>
                            <div class="row">
                                <div class="col-3 col-md-3 col-xl-3 col-sm-3 mb-3 form-group">
                                    <label for="" class="form-control-label">
                                        Nombre:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control required" id="nombre" name="nombre">
                                </div>
                                <div class="col-3 col-md-3 col-xl-3 col-sm-3 mb-3 form-group">
                                    <label for="" class="form-control-label">
                                        Apellido: 
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control required" id="apellido" name="apellido">
                                </div>
                                <div class="col-3 col-md-3 col-xl-3 col-sm-3 mb-3 form-group">
                                    <label for="" class="form-control-label">
                                        Telefono:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control required" id="telefono" name="telefono">
                                </div>
                                <div class="col-3 col-md-3 col-xl-3 col-sm-3 mb-3 form-group">
                                    <label for="" class="form-control-label">
                                        Correo:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control required" id="correo" name="correo">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12 col-sm-12 mb-5">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Observaciones:</label>
                                <textarea class="form-control required" id="observaciones" name="observaciones" rows="5" style="resize:none"></textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- Step 3 -->
            <h6>Confirmar datos</h6>
            <section>
                <div class="row mt-3 mb-3">
                    <div class="col-12 col-xl-12 col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%">
                                <caption><h5>Categoria del reporte</h5></caption>
                                <thead class="text-center">
                                    <tr>
                                        <th>Categoria</th>
                                        <th>Subcategoria</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td><span id="conf_cat"></span></td>
                                        <td><span id="conf_Subcat"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-xl-12 col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%">
                                <caption><h5>Datos de ubicacion</h5></caption>
                                <thead class="text-center">
                                    <tr>
                                        <th>Acueducto</th>
                                        <th>Municipio</th>
                                        <th>Parroquia</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td><span id="conf_Acueducto"></span></td>
                                        <td><span id="conf_Municipio"></span></td>
                                        <td><span id="conf_Parroquia"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-xl-12 col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%">
                                <thead class="text-center">
                                    <tr>
                                        <th>Sector/urb</th>
                                        <th>Av/calle</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td><span id="conf_Sector"></span></td>
                                        <td><span id="conf_Av"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-xl-12 col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%">
                                <thead class="text-center">
                                    <tr>
                                        <th>Nombre de Inmueble</th>
                                        <th>Punto de Referencia</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td><span id="conf_Nombre_Inmueble"></span></td>
                                        <td><span id="conf_Punto_Referencia"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-xl-12 col-md-12 col-sm-12 text-center">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%">
                                <caption><h5>Datos del cliente</h5></caption>
                                <thead class="text-center">
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td><span id="conf_Nombre"></span></td>
                                        <td><span id="conf_Apellido"></span></td>
                                        <td><span id="conf_Telefono"></span></td>
                                        <td><span id="conf_Correo"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                   
                    <div class="col-12 col-xl-12 col-md-12 col-sm-12 text-center">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <td><h5>Observacion</h5></td>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td><p id="conf_observacion"></p></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <style>
	    .help-block {
	        color: #a94442;
	        display: inline;
	        font-weight: bolder;
	    }
	</style>
	<script type="text/javascript">
		var form = $(".validation-wizard").show();
		function search_municipio(input_origen,input_destino){
		    var input_origen = input_origen.value;
		    $.ajax({
		        type: "POST",
		        url: "/potable/SearchData",
		        async: false,
		        cache: false,
		        data: {
		        	"_token": "{{ csrf_token() }}",
		            condicion: "id_acueducto="+input_origen,
		            table: 'municipio',
		            campo: '*'
		        },
		        success: function(response){
		            var response = $.parseJSON(response);
		            input_destino.empty();
		            input_destino.append("<option value='null' >Seleccione una opcion</option>");
		            for (var i = 0; i < response.length; i++) {
		                input_destino.append("<option value='"+response[i].id_mpio+"' >"+response[i].nom_mpio+"</option>");
		            }
		        }
		    });
		}
		function search_parroquia(input_origen,input_destino){
		    var input_origen = input_origen.value;
		    $.ajax({
		        type: "POST",
		        url: "/potable/SearchData",
		        async: false,
		        cache: false,
		        data: {
		        	"_token": "{{ csrf_token() }}",
		            condicion: "id_mpio="+input_origen,
		            table: 'parroquias',
		            campo: '*'
		        },
		        success: function(response){
		            var response = $.parseJSON(response);
		            input_destino.empty();
		            input_destino.append("<option value='null' >Seleccione una opcion</option>");
		            for (var i = 0; i < response.length; i++) {
		                input_destino.append("<option value='"+response[i].id_parroq+"' >"+response[i].nom_parroq+"</option>");
		            }
		        }
		    });
		}
		function search_categ(input) {
		    var input = input.value;
		    var select = $("#subCateg");
		   $.ajax({
		        type: "POST",
		        url: "/potable/listarSubcategoria",
		        async: false,
		        cache: false,
		        data: {
		        	"_token": "{{ csrf_token() }}",
		            q: input
		        },
		        success: function(response){
		            var response = $.parseJSON(response);
		            select.empty();
		            select.append("<option value='null' >Seleccione una opcion</option>");
		            for (var i = 0; i < response.length; i++) {
		                select.append("<option value='"+response[i].id_categ+"' >"+response[i].nom_subcateg+"</option>");
		            }
		        }
		    });
		}
		function search_cliente(nic){
		    var nic = nic[0].value;
		    $.ajax({
		        type: "POST",
		        url: "/potable/SearchClient",
		        data: {
		        	"_token": "{{ csrf_token() }}",
		            nic:nic
		        },
		        success: function(response){
		            var response = $.parseJSON(response);
		            if(response != null && Object.keys(response).length > 0){
		                response = response[0];
		                $("#apellido").val(response.apellido).attr('readonly','true');
		                $("#correo").val(response.correo).attr('readonly','true');
		                $("#nom_inmueble").val(response.nom_inmueble).attr('readonly','true');
		                $("#nombre").val(response.nombre).attr('readonly','true');
		                $("#punto_referencia").val(response.pnto_ref).attr('readonly','true');
		                $("#telefono").val(response.telefono).attr('readonly','true');
		                $("#id_acueducto option[value="+response.id_acueducto+"]").attr("selected",true);
		                $("#id_acueducto").attr('onfocus','this.blur()');
		                $("#id_acueducto").attr('readonly','true');
		                search_municipio($("#id_acueducto")[0],$('#id_municipio'));
		                $("#id_municipio option[value="+response.id_mpio+"]").attr("selected",true);
		                $("#id_municipio").attr('onfocus','this.blur()');
		                $("#id_municipio").attr('readonly','true');
		                search_parroquia($('#id_municipio')[0],$('#id_parroquia'));
		                $("#id_parroquia option[value="+response.id_parroq+"]").attr("selected",true);
		                $("#id_parroquia").attr('onfocus','this.blur()');
		                $("#id_parroquia").attr('readonly','true');
		                $("#sector").val(response.nom_sector).attr('readonly','true');
		                $("#av").val(response.nom_av_calle).attr('readonly','true');
		            }
		            
		        }
		    });
		}

		var form = $(".validation-wizard").show();

		$(".validation-wizard").steps({
		    headerTag: "h6",
		    bodyTag: "section",
		    transitionEffect: "fade",
		    titleTemplate: '<span class="step">#index#</span> #title#',
		    labels: {
		        next: "Siguiente",
		        previous: "Previo",
		        finish: "Registrar"
		    },
		    onStepChanging: function (event, currentIndex, newIndex) {
		        if(newIndex==2){
		            var categoria = $("#categoria")[0];
		            var subcate = $("#subCateg")[0];
		            var acueducto= $("#id_acueducto")[0];
		            var municipio= $("#id_municipio")[0];
		            var parroquia= $("#id_parroquia")[0];
		            for (var i = 0; i < acueducto.length; i++) {
		                if(acueducto.value == acueducto[i].value){
		                    $("#conf_Acueducto").html(acueducto[i].innerHTML);
		                } 
		            }
		            for (var i = 0; i < municipio.length; i++) {
		                if(municipio.value == municipio[i].value){
		                    $("#conf_Municipio").html(municipio[i].innerHTML);
		                } 
		            }
		            for (var i = 0; i < parroquia.length; i++) {
		                if(parroquia.value == parroquia[i].value){
		                    $("#conf_Parroquia").html(parroquia[i].innerHTML);
		                } 
		            }
		            for (var i = 0; i < categoria.length; i++) {
		                if(categoria.value == categoria[i].value){
		                    $("#conf_cat").html(categoria[i].innerHTML);
		                } 
		            }
		            for (var i = 0; i < subcate.length; i++) {
		                if(subcate.value == subcate[i].value){
		                    $("#conf_Subcat").html(subcate[i].innerHTML);
		                } 
		            }
		            $("#conf_Nombre").html($("#nombre")[0].value);
		            $("#conf_Apellido").html($("#apellido")[0].value);
		            $("#conf_Telefono").html($("#telefono")[0].value);
		            $("#conf_Correo").html($("#correo")[0].value);
		            $("#conf_observacion").html($("#observaciones")[0].value);
		            $("#conf_Nombre_Inmueble").html($("#nom_inmueble")[0].value);
		            $("#conf_Punto_Referencia").html($("#punto_referencia")[0].value);
		            $("#conf_Sector").html($("#sector")[0].value);
		            $("#conf_Av").html($("#av")[0].value);
		        }
		        return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
		    },
		    onFinishing: function (event, currentIndex) {
		        return form.validate().settings.ignore = ":disabled", form.valid()
		    },
		    onFinished: function (event, currentIndex) {
		        var form_data = $("#reporte").serialize();
		        form_data +="&_token={{ csrf_token() }}";
		        $.ajax({
		            url: '/potable/registerReport',
		            type: 'POST',
		            data: form_data,
		            success:(response)=>{
		            	console.log(response);
		                var response = $.parseJSON(response);
		                var options = new Object();
		                if (!response.error){
		                    options.title="Datos registrado con exito";
		                    options.text=response.message+response.code;
		                    options.html="<p>"+response.message+"<b>"+response.code+"</b></p>";
		                    options.icon="success";
		                }else{
		                    options.title="Los datos no pudieron ser registrado";
		                    options.text=response.message;
		                    options.html="<p>"+response.message+"</p>";
		                    options.icon="error";
		                }
		                swal(options).then((e)=>{
		                    if(e){
		                        document.location.reload();
		                    }
		                });
		            }
		        });   
		    }
		})
		$(".validation-wizard").validate({
		    ignore: "input[type=hidden]",
		    errorClass: "text-danger",
		    successClass: "text-success",
		    highlight: function (element, errorClass) {
		        $(element).removeClass(errorClass)
		    },
		    unhighlight: function (element, errorClass) {
		        $(element).removeClass(errorClass)
		    },
		    errorPlacement: function (error, element) {
		        $(error).addClass('help-block');
		        error.insertAfter(element)
		    },
		    rules: {
		        number:{
		            categoria: !0,
		            subCateg: !0
		        }
		    }
		})
	</script>
@endsection