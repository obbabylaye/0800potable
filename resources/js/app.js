require('./bootstrap');
import DataTable from  'laravel-vue-datatable';
import axios from 'axios';
import VueSweetalert2 from 'vue-sweetalert2';
import VueFormWizard from 'vue-form-wizard';
import VueStepWizard from 'vue-step-wizard';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';

window.Vue = require('vue').default;

Vue.use(DataTable);
Vue.use(VueSweetalert2);
Vue.use(VueFormWizard);
Vue.use(VueStepWizard);


Vue.component(
	'chat-component',
	require('./components/ChatComponent.vue').default
);
Vue.component(
	'index-component',
	require('./components/IndexComponent.vue').default
);
Vue.component(
	'reportform-component',
	require('./components/ReportformComponent.vue').default
);
Vue.component(
	'list-component',
	require('./components/ListComponent.vue').default
);
const app = new Vue({
    el: '#app',
});
