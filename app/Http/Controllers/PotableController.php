<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Reporte;

class PotableController extends Controller
{

    public function SearchClient()
    {
        $query = DB::table('clientes')
                    ->join('direcciones','clientes.id_direccion','=','direcciones.id_direccion')
                    ->join('sectores','direcciones.id_sector','=','sectores.id_sector')
                    ->join('av_calle','av_calle.id_sector','=','sectores.id_sector')
                    ->WHERE('clientes.nic','=',intval($_POST['nic']))
                    ->select('clientes.apellido','clientes.correo','clientes.nom_inmueble','clientes.nombre','clientes.pnto_ref',
                    'clientes.telefono','direcciones.id_acueducto','direcciones.id_mpio','direcciones.id_parroq',
                    'direcciones.id_sector','direcciones.id_av_calle','sectores.nom_sector','av_calle.nom_av_calle')
                    ->get();
        return json_encode($query);
    }

    public function SearchData()
    {
        $condicion=explode("=",$_POST['condicion']);
        $query = DB::table($_POST['table'])
                    ->WHERE($condicion[0],"=",$condicion[1])
                    ->select($_POST['campo'])->get();
        return json_encode($query);
    }
    public function listarSubcategoria()
    {
        return json_encode(DB::table('sub_categorias')->where('id_categ',"=",$_POST['q'])->get());
    }

    public function new()
    {
        $categoria= DB::table('categorias')->get();
        $acueducto= DB::table('acueductos')->get();
        return view('dashboard.formReport',['categoria'=>$categoria,"acueducto"=>$acueducto]);
    }

    public function registerReport()
    {
        $data=NULL;
        $data_cliente=NULL;
        $data_direccion=NULL;
        $data_av=NULL;
        $data_sector=NULL;
        $interactive=NULL;
        $result=NULL;
        if (!empty($_POST)) {
            $data['fecha_crea']= DB::raw('current_date');
            $data['hora_crea']= Carbon::now()->toTimeString();
            $data['id_us']=intval(session('id_user'));
            foreach ($_POST as $key => $value) {
                switch ($key) {
                    case 'av':
                        $data_av['nom_av_calle']=$value;
                        break;
                    case 'sector':
                        $data_sector['nom_sector']=$value;
                        $data_sector['id_parroq']=intval($_POST['id_parroquia']);
                        break;
                    case 'id_acueducto':
                        $data_direccion['id_acueducto']=intval($value);
                        break;
                    case 'id_municipio':
                        $data_direccion['id_mpio']=intval($value);
                        break;
                    case 'id_parroquia':
                        $data_direccion['id_parroq']=intval($value);
                        break;
                    case 'categoria':
                        $data['id_categ']=intval($value);
                        break;
                    case 'nic':
                        if ($value == 1) {
                            $data_cliente['nic']=intval(NULL);
                        }else{
                            $data_cliente['nic']=intval($_POST['nic']);
                        }
                        break;
                    case 'nom_inmueble':
                        $data_cliente['nom_inmueble']=$value;
                        break;
                    case 'subCateg':
                        $data['id_sub_categ']=intval($value);
                        break;
                    case 'usuario':
                        $data[$key]=intval($value);
                        break;
                    case 'nombre':
                        $data_cliente[$key]=$value;
                        break;
                    case 'telefono':
                        $data_cliente[$key]=intval($value);
                        break;
                    case 'apellido':
                        $data_cliente[$key]=$value;
                        break;
                    case 'correo':
                        $data_cliente[$key]=$value;
                        break;
                    case 'punto_referencia':
                        $data_cliente['pnto_ref']=$value;
                        break;
                    default:
                        if ($key <> "_token") {
                            $data[$key]=$value;
                        }
                        break;
                }
            }
            $select_cliente=DB::table('clientes')
                    ->where('nic',"=",$data_cliente['nic'])
                    ->select('id_cliente','id_direccion')->first();
            $cod =date("ymd").random_int(0000, 9999);
            $data['cod_reporte']=intval($cod);
            $data['id_estatu']=intval('1');
            if (strlen($cod)<10) {
                $cod=$cod."0";
            }
            //var_dump($data);
            //die;
            if (!empty($select_cliente) && !is_null($select_cliente)) {
                $data['id_cliente']=intval($select_cliente->id_cliente);
                $data['id_direccion']=intval($select_cliente->id_direccion);
                $insert_report=DB::table('reportes')->insert($data);
                if ($insert_report) {
                    $interactive=1;
                }else{
                    $interactive=2;
                }
            }else{
                $sector=DB::table('sectores')
                    ->where('nom_sector',"=",$data_sector['nom_sector'])
                    ->select('id_sector')
                    ->first();
                $av=DB::table('av_calle')
                    ->where('nom_av_calle',"=",$data_av['nom_av_calle'])
                    ->select('id_av_calle')
                    ->first();
                
                if (!empty($sector) && !is_null($sector)) {
                    $data_av['id_sector']=$sector->id_sector;
                    $insert_sector=true;
                }else{
                    $insert_sector=DB::table('sectores')->insert($data_sector);
                    $max_sector=DB::table('sectores')->max('id_sector')->get();
                    $data_av['id_sector']=intval($max_sector);
                    $data_direccion['id_sector']=intval($max_sector);
                    $insert_sector=true;
                }

                if (!empty($av) && !is_null($av)) {
                    $data_av['id_sector']=$av->id_av_calle;
                    $insert_av=true;
                }else{
                    $insert_av=DB::table('av_calle')->insert($data_av);
                    $max_av=DB::table('av_calle')->max('id_av_calle')->get();
                    $data_av['id_av_calle']=intval($max_av);
                    $data_direccion['id_av_calle']=intval($max_av);
                    $insert_av=true;
                }
                if($insert_sector){
                    if ($insert_av) {
                        $insert_direccion=DB::table('direcciones')->insert($data_direccion);
                        if($insert_direccion){
                            $max_direccion=DB::table('direcciones')->max('id_direccion');
                            $data_cliente['id_direccion']=intval($max_direccion);
                            $insert_cliente=DB::table('clientes')->insert($data_cliente);
                            if ($insert_cliente) {
                                $max_cliente=DB::table('clientes')->max('id_cliente');
                                $data['id_cliente']=intval($max_cliente);
                                $data['id_direccion']=intval($data_cliente['id_direccion']);
                                $insert_report=DB::table('reportes')->insert($data);
                                if ($insert_report) {
                                    $interactive=1;
                                }else{
                                    $interactive=2;
                                }
                            }
                        }
                    }
                }
            }

            switch ($interactive) {
                case 1:
                    $result['code']= $cod;
                    $result['error']= FALSE;
                    $result['message']= "Informacion registrada con exito su codigo es ";
                    break;
                case 2:
                    $result['code']= null;
                    $result['error']= TRUE;
                    $result['message']= "Por favor verifique la informacion ingresada";
                    break;
            }
            return json_encode($result);
        }
    }

    public function list()
    {
        $data= DB::table('reportes')
            ->join('usuarios','reportes.id_usu','=','usuarios.id_usu')
            ->select('fecha_crea','cod_reporte','id_us','id_estatu','hora_crea')
            ->get();
        $table = DB::table('information_schema.columns')
                    ->select('column_name', 'data_type')
                    ->where('table_name','=','reportes')->get();

        return view('dashboard.list',['data'=>$data,'table'=>$table]);
    }

    public function Datalist()
    {
        $query= DB::table('reportes')->get();
        return json_encode($query);
    }

}
