<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $errors = array();
        $result = array();
        $status = 200;
        if($request->ajax()){
            if ($request->isMethod('post')) {
                if (!empty($request->password) && !empty($request->usuario)) {
                    $user = DB::table('data_us')
                        ->join('usuarios','usuarios.id_data_us','=','data_us.id_data_us')
                        ->join('roles','usuarios.id_rol','=','roles.id_rol')
                        ->select('data_us.id_data_us','data_us.nombre','data_us.correo','usuarios.nom_us', 'usuarios.clave', 'usuarios.estatus','roles.id_rol')
                        ->where("nom_us","=",$request->usuario)->get()->first();
                    if (password_verify($request->password, $user->clave)) {
                        session([
                            'id_user' => $user->id_data_us,
                            'usuario' => $user->nom_us,
                            'rol' => $user->id_rol
                        ]);
                        $result['success']=true;
                    }else{
                        $result['success']=false;
                        $errors['password']='Contraseña incorrecta por favor verificar';
                        $errors['class']='is-invalid';
                        $result['errors']=$errors;
                        $status=422;
                    }    
                }else{
                    if (empty($request->password)) {
                        $errors['password']='Campo requerido';
                        $errors['class']='is-invalid';
                    }elseif (empty($request->usuario)) {
                        $errors['usuario']='Campo requerido';
                        $errors['class']='is-invalid';
                    }
                    $result['success']=false;
                    $result['errors']=$errors;
                    $status=422;
                }
               
                return response($result,$status);
                //return response(dd($data));
                //return response($data);
            }
        }
        
        
        //return response()->json([$request->all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
