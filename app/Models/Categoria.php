<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Categoria
 * 
 * @property int $id_categ
 * @property character varying|null $nom_categ
 * 
 * @property Collection|Reporte[] $reportes
 * @property Collection|SubCategoria[] $sub_categorias
 *
 * @package App\Models
 */
class Categoria extends Model
{
	protected $table = 'categorias';
	protected $primaryKey = 'id_categ';
	public $timestamps = false;

	protected $casts = [
		'nom_categ' => 'character varying'
	];

	protected $fillable = [
		'nom_categ'
	];

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'id_categ');
	}

	public function sub_categorias()
	{
		return $this->hasMany(SubCategoria::class, 'id_categ');
	}
}
