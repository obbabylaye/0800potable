<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HistoricoReport
 * 
 * @property int $id_hist_report
 * @property Carbon|null $fecha_hist
 * @property timestamp without time zone|null $hora__hist
 * @property int|null $id_reporte
 * @property int|null $id_hreport
 * @property int|null $id_modificacion
 * @property int|null $id_us
 * 
 * @property Reporte|null $reporte
 * @property Hreporte|null $hreporte
 * @property ModificacionesReport|null $modificaciones_report
 * @property Usuario|null $usuario
 *
 * @package App\Models
 */
class HistoricoReport extends Model
{
	protected $table = 'historico_report';
	protected $primaryKey = 'id_hist_report';
	public $timestamps = false;

	protected $casts = [
		'hora__hist' => 'timestamp without time zone',
		'id_reporte' => 'int',
		'id_hreport' => 'int',
		'id_modificacion' => 'int',
		'id_us' => 'int'
	];

	protected $dates = [
		'fecha_hist'
	];

	protected $fillable = [
		'fecha_hist',
		'hora__hist',
		'id_reporte',
		'id_hreport',
		'id_modificacion',
		'id_us'
	];

	public function reporte()
	{
		return $this->belongsTo(Reporte::class, 'id_reporte');
	}

	public function hreporte()
	{
		return $this->belongsTo(Hreporte::class, 'id_hreport');
	}

	public function modificaciones_report()
	{
		return $this->belongsTo(ModificacionesReport::class, 'id_modificacion');
	}

	public function usuario()
	{
		return $this->belongsTo(Usuario::class, 'id_us');
	}
}
