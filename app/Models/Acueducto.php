<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Acueducto
 * 
 * @property int $id_acueducto
 * @property character varying|null $nom_acueducto
 * @property int|null $cod_hidrosgc
 * 
 * @property Collection|Municipio[] $municipios
 * @property Collection|Departamento[] $departamentos
 * @property Collection|Direccione[] $direcciones
 *
 * @package App\Models
 */
class Acueducto extends Model
{
	protected $table = 'acueductos';
	protected $primaryKey = 'id_acueducto';
	public $timestamps = false;

	protected $casts = [
		'nom_acueducto' => 'character varying',
		'cod_hidrosgc' => 'int'
	];

	protected $fillable = [
		'nom_acueducto',
		'cod_hidrosgc'
	];

	public function municipios()
	{
		return $this->hasMany(Municipio::class, 'id_acueducto');
	}

	public function departamentos()
	{
		return $this->hasMany(Departamento::class, 'id_acueducto');
	}

	public function direcciones()
	{
		return $this->hasMany(Direccione::class, 'id_acueducto');
	}
}
