<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AvCalle
 * 
 * @property int $id_av_calle
 * @property int $id_sector
 * @property character varying|null $nom_av_calle
 * 
 * @property Sectore $sectore
 * @property Collection|Direccione[] $direcciones
 *
 * @package App\Models
 */
class AvCalle extends Model
{
	protected $table = 'av_calle';
	protected $primaryKey = 'id_av_calle';
	public $timestamps = false;

	protected $casts = [
		'id_sector' => 'int',
		'nom_av_calle' => 'character varying'
	];

	protected $fillable = [
		'id_sector',
		'nom_av_calle'
	];

	public function sectore()
	{
		return $this->belongsTo(Sectore::class, 'id_sector');
	}

	public function direcciones()
	{
		return $this->hasMany(Direccione::class, 'id_av_calle');
	}
}
