<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 * 
 * @property int $id_area
 * @property int|null $cod_area
 *
 * @package App\Models
 */
class Area extends Model
{
	protected $table = 'areas';
	protected $primaryKey = 'id_area';
	public $timestamps = false;

	protected $casts = [
		'cod_area' => 'int'
	];

	protected $fillable = [
		'cod_area'
	];
}
