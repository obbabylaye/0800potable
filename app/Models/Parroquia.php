<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Parroquia
 * 
 * @property int $id_parroq
 * @property character varying|null $nom_parroq
 * @property int|null $id_mpio
 * 
 * @property Municipio|null $municipio
 * @property Collection|Sectore[] $sectores
 * @property Collection|Direccione[] $direcciones
 *
 * @package App\Models
 */
class Parroquia extends Model
{
	protected $table = 'parroquias';
	protected $primaryKey = 'id_parroq';
	public $timestamps = false;

	protected $casts = [
		'nom_parroq' => 'character varying',
		'id_mpio' => 'int'
	];

	protected $fillable = [
		'nom_parroq',
		'id_mpio'
	];

	public function municipio()
	{
		return $this->belongsTo(Municipio::class, 'id_mpio');
	}

	public function sectores()
	{
		return $this->hasMany(Sectore::class, 'id_parroq');
	}

	public function direcciones()
	{
		return $this->hasMany(Direccione::class, 'id_parroq');
	}
}
