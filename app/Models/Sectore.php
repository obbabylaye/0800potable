<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sectore
 * 
 * @property int $id_sector
 * @property character varying|null $nom_sector
 * @property int|null $id_parroq
 * 
 * @property Parroquia|null $parroquia
 * @property Collection|Direccione[] $direcciones
 * @property Collection|AvCalle[] $av_calles
 *
 * @package App\Models
 */
class Sectore extends Model
{
	protected $table = 'sectores';
	protected $primaryKey = 'id_sector';
	public $timestamps = false;

	protected $casts = [
		'nom_sector' => 'character varying',
		'id_parroq' => 'int'
	];

	protected $fillable = [
		'nom_sector',
		'id_parroq'
	];

	public function parroquia()
	{
		return $this->belongsTo(Parroquia::class, 'id_parroq');
	}

	public function direcciones()
	{
		return $this->hasMany(Direccione::class, 'id_sector');
	}

	public function av_calles()
	{
		return $this->hasMany(AvCalle::class, 'id_sector');
	}
}
