<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Direccione
 * 
 * @property int $id_direccion
 * @property int|null $id_acueducto
 * @property int|null $id_mpio
 * @property int|null $id_parroq
 * @property int|null $id_sector
 * @property int|null $id_av_calle
 * 
 * @property Acueducto|null $acueducto
 * @property Municipio|null $municipio
 * @property Parroquia|null $parroquia
 * @property Sectore|null $sectore
 * @property AvCalle|null $av_calle
 * @property Collection|Reporte[] $reportes
 * @property Collection|Cliente[] $clientes
 * @property Collection|Anexo[] $anexos
 *
 * @package App\Models
 */
class Direccione extends Model
{
	protected $table = 'direcciones';
	protected $primaryKey = 'id_direccion';
	public $timestamps = false;

	protected $casts = [
		'id_acueducto' => 'int',
		'id_mpio' => 'int',
		'id_parroq' => 'int',
		'id_sector' => 'int',
		'id_av_calle' => 'int'
	];

	protected $fillable = [
		'id_acueducto',
		'id_mpio',
		'id_parroq',
		'id_sector',
		'id_av_calle'
	];

	public function acueducto()
	{
		return $this->belongsTo(Acueducto::class, 'id_acueducto');
	}

	public function municipio()
	{
		return $this->belongsTo(Municipio::class, 'id_mpio');
	}

	public function parroquia()
	{
		return $this->belongsTo(Parroquia::class, 'id_parroq');
	}

	public function sectore()
	{
		return $this->belongsTo(Sectore::class, 'id_sector');
	}

	public function av_calle()
	{
		return $this->belongsTo(AvCalle::class, 'id_av_calle');
	}

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'id_direccion');
	}

	public function clientes()
	{
		return $this->hasMany(Cliente::class, 'id_direccion');
	}

	public function anexos()
	{
		return $this->hasMany(Anexo::class, 'id_direccion');
	}
}
