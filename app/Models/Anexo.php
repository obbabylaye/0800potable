<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Anexo
 * 
 * @property int $id_anexo
 * @property character varying|null $nombre
 * @property character varying|null $apellido
 * @property character varying|null $observacion
 * @property int $id_direccion
 * 
 * @property Direccione $direccione
 * @property Collection|Hreporte[] $hreportes
 *
 * @package App\Models
 */
class Anexo extends Model
{
	protected $table = 'anexos';
	protected $primaryKey = 'id_anexo';
	public $timestamps = false;

	protected $casts = [
		'nombre' => 'character varying',
		'apellido' => 'character varying',
		'observacion' => 'character varying',
		'id_direccion' => 'int'
	];

	protected $fillable = [
		'nombre',
		'apellido',
		'observacion',
		'id_direccion'
	];

	public function direccione()
	{
		return $this->belongsTo(Direccione::class, 'id_direccion');
	}

	public function hreportes()
	{
		return $this->hasMany(Hreporte::class, 'id_anexo');
	}
}
