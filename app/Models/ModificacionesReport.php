<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ModificacionesReport
 * 
 * @property int $id_modificacion
 * @property Carbon|null $fecha_modif
 * @property timestamp without time zone|null $hora_modif
 * @property character varying|null $observaciones
 * @property int $id_us
 * @property int $id_reporte
 * 
 * @property Usuario $usuario
 * @property Reporte $reporte
 * @property Collection|HistoricoReport[] $historico_reports
 *
 * @package App\Models
 */
class ModificacionesReport extends Model
{
	protected $table = 'modificaciones_report';
	protected $primaryKey = 'id_modificacion';
	public $timestamps = false;

	protected $casts = [
		'hora_modif' => 'timestamp without time zone',
		'observaciones' => 'character varying',
		'id_us' => 'int',
		'id_reporte' => 'int'
	];

	protected $dates = [
		'fecha_modif'
	];

	protected $fillable = [
		'fecha_modif',
		'hora_modif',
		'observaciones',
		'id_us',
		'id_reporte'
	];

	public function usuario()
	{
		return $this->belongsTo(Usuario::class, 'id_us');
	}

	public function reporte()
	{
		return $this->belongsTo(Reporte::class, 'id_reporte');
	}

	public function historico_reports()
	{
		return $this->hasMany(HistoricoReport::class, 'id_modificacion');
	}
}
