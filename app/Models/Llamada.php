<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Llamada
 * 
 * @property int $id_llamada
 * @property Carbon|null $fecha_call
 * @property timestamp without time zone|null $tiempo_call
 * @property character varying|null $observaciones
 * @property int $id_tipo_llamada
 * 
 * @property TipoLlamada $tipo_llamada
 *
 * @package App\Models
 */
class Llamada extends Model
{
	protected $table = 'llamadas';
	protected $primaryKey = 'id_llamada';
	public $timestamps = false;

	protected $casts = [
		'tiempo_call' => 'timestamp without time zone',
		'observaciones' => 'character varying',
		'id_tipo_llamada' => 'int'
	];

	protected $dates = [
		'fecha_call'
	];

	protected $fillable = [
		'fecha_call',
		'tiempo_call',
		'observaciones',
		'id_tipo_llamada'
	];

	public function tipo_llamada()
	{
		return $this->belongsTo(TipoLlamada::class, 'id_tipo_llamada');
	}
}
