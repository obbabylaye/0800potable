<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Estatus
 * 
 * @property int $id_estatu
 * @property character varying|null $descrip_estatu
 * 
 * @property Collection|Reporte[] $reportes
 *
 * @package App\Models
 */
class Estatus extends Model
{
	protected $table = 'estatus';
	protected $primaryKey = 'id_estatu';
	public $timestamps = false;

	protected $casts = [
		'descrip_estatu' => 'character varying'
	];

	protected $fillable = [
		'descrip_estatu'
	];

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'id_estatu');
	}
}
