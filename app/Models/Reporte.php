<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reporte
 * 
 * @property int $id_reporte
 * @property Carbon|null $fecha_crea
 * @property time without time zone|null $hora_crea
 * @property character varying|null $observaciones
 * @property int $id_categ
 * @property int $id_sub_categ
 * @property int $id_cliente
 * @property int $id_us
 * @property int|null $id_direccion
 * @property int|null $cod_reporte
 * @property int $id_estatu
 * 
 * @property Categoria $categoria
 * @property SubCategoria $sub_categoria
 * @property Cliente $cliente
 * @property Usuario $usuario
 * @property Direccione|null $direccione
 * @property Estatus $estatus
 * @property Collection|HistoricoReport[] $historico_reports
 * @property Collection|ModificacionesReport[] $modificaciones_reports
 * @property Collection|Hreporte[] $hreportes
 *
 * @package App\Models
 */
class Reporte extends Model
{
	protected $table = 'reportes';
	protected $primaryKey = 'id_reporte';
	public $timestamps = false;

	protected $casts = [
		'hora_crea' => 'time without time zone',
		'observaciones' => 'character varying',
		'id_categ' => 'int',
		'id_sub_categ' => 'int',
		'id_cliente' => 'int',
		'id_us' => 'int',
		'id_direccion' => 'int',
		'cod_reporte' => 'int',
		'id_estatu' => 'int'
	];

	protected $dates = [
		'fecha_crea'
	];

	protected $fillable = [
		'fecha_crea',
		'hora_crea',
		'observaciones',
		'id_categ',
		'id_sub_categ',
		'id_cliente',
		'id_us',
		'id_direccion',
		'cod_reporte',
		'id_estatu'
	];

	public function categoria()
	{
		return $this->belongsTo(Categoria::class, 'id_categ');
	}

	public function sub_categoria()
	{
		return $this->belongsTo(SubCategoria::class, 'id_sub_categ');
	}

	public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'id_cliente');
	}

	public function usuario()
	{
		return $this->belongsTo(Usuario::class, 'id_us');
	}

	public function direccione()
	{
		return $this->belongsTo(Direccione::class, 'id_direccion');
	}

	public function estatus()
	{
		return $this->belongsTo(Estatus::class, 'id_estatu');
	}

	public function historico_reports()
	{
		return $this->hasMany(HistoricoReport::class, 'id_reporte');
	}

	public function modificaciones_reports()
	{
		return $this->hasMany(ModificacionesReport::class, 'id_reporte');
	}

	public function hreportes()
	{
		return $this->hasMany(Hreporte::class, 'id_reporte');
	}
}
