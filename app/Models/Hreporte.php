<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Hreporte
 * 
 * @property int $id_hreport
 * @property Carbon|null $fecha_hr
 * @property time without time zone|null $hora_hr
 * @property int|null $id_us
 * @property int|null $id_reporte
 * @property int|null $id_anexo
 * @property int|null $cod_hreport
 * 
 * @property Usuario|null $usuario
 * @property Reporte|null $reporte
 * @property Anexo|null $anexo
 * @property Collection|HistoricoReport[] $historico_reports
 *
 * @package App\Models
 */
class Hreporte extends Model
{
	protected $table = 'hreportes';
	protected $primaryKey = 'id_hreport';
	public $timestamps = false;

	protected $casts = [
		'hora_hr' => 'time without time zone',
		'id_us' => 'int',
		'id_reporte' => 'int',
		'id_anexo' => 'int',
		'cod_hreport' => 'int'
	];

	protected $dates = [
		'fecha_hr'
	];

	protected $fillable = [
		'fecha_hr',
		'hora_hr',
		'id_us',
		'id_reporte',
		'id_anexo',
		'cod_hreport'
	];

	public function usuario()
	{
		return $this->belongsTo(Usuario::class, 'id_us');
	}

	public function reporte()
	{
		return $this->belongsTo(Reporte::class, 'id_reporte');
	}

	public function anexo()
	{
		return $this->belongsTo(Anexo::class, 'id_anexo');
	}

	public function historico_reports()
	{
		return $this->hasMany(HistoricoReport::class, 'id_hreport');
	}
}
