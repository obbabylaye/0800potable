<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoLlamada
 * 
 * @property int $id_tipo_llamada
 * @property character varying|null $descripcion
 * 
 * @property Collection|Llamada[] $llamadas
 *
 * @package App\Models
 */
class TipoLlamada extends Model
{
	protected $table = 'tipo_llamada';
	protected $primaryKey = 'id_tipo_llamada';
	public $timestamps = false;

	protected $casts = [
		'descripcion' => 'character varying'
	];

	protected $fillable = [
		'descripcion'
	];

	public function llamadas()
	{
		return $this->hasMany(Llamada::class, 'id_tipo_llamada');
	}
}
