<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoVium
 * 
 * @property int $id_tipo_via
 * @property character varying|null $descrip_via
 * @property character varying|null $tipo_via
 *
 * @package App\Models
 */
class TipoVium extends Model
{
	protected $table = 'tipo_via';
	protected $primaryKey = 'id_tipo_via';
	public $timestamps = false;

	protected $casts = [
		'descrip_via' => 'character varying',
		'tipo_via' => 'character varying'
	];

	protected $fillable = [
		'descrip_via',
		'tipo_via'
	];
}
