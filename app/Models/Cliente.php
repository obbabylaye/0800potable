<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cliente
 * 
 * @property int $id_cliente
 * @property character varying|null $nombre
 * @property character varying|null $apellido
 * @property float|null $nic
 * @property float|null $telefono
 * @property character varying|null $pnto_ref
 * @property character varying|null $correo
 * @property int $id_direccion
 * @property character varying|null $nom_inmueble
 * 
 * @property Direccione $direccione
 * @property Collection|Reporte[] $reportes
 *
 * @package App\Models
 */
class Cliente extends Model
{
	protected $table = 'clientes';
	protected $primaryKey = 'id_cliente';
	public $timestamps = false;

	protected $casts = [
		'nombre' => 'character varying',
		'apellido' => 'character varying',
		'nic' => 'float',
		'telefono' => 'float',
		'pnto_ref' => 'character varying',
		'correo' => 'character varying',
		'id_direccion' => 'int',
		'nom_inmueble' => 'character varying'
	];

	protected $fillable = [
		'nombre',
		'apellido',
		'nic',
		'telefono',
		'pnto_ref',
		'correo',
		'id_direccion',
		'nom_inmueble'
	];

	public function direccione()
	{
		return $this->belongsTo(Direccione::class, 'id_direccion');
	}

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'id_cliente');
	}
}
