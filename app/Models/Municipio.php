<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Municipio
 * 
 * @property int $id_mpio
 * @property character varying|null $nom_mpio
 * @property int $id_acueducto
 * @property int $id_depto
 * 
 * @property Acueducto $acueducto
 * @property Departamento $departamento
 * @property Collection|Parroquia[] $parroquias
 * @property Collection|Direccione[] $direcciones
 *
 * @package App\Models
 */
class Municipio extends Model
{
	protected $table = 'municipio';
	protected $primaryKey = 'id_mpio';
	public $timestamps = false;

	protected $casts = [
		'nom_mpio' => 'character varying',
		'id_acueducto' => 'int',
		'id_depto' => 'int'
	];

	protected $fillable = [
		'nom_mpio',
		'id_acueducto',
		'id_depto'
	];

	public function acueducto()
	{
		return $this->belongsTo(Acueducto::class, 'id_acueducto');
	}

	public function departamento()
	{
		return $this->belongsTo(Departamento::class, 'id_depto');
	}

	public function parroquias()
	{
		return $this->hasMany(Parroquia::class, 'id_mpio');
	}

	public function direcciones()
	{
		return $this->hasMany(Direccione::class, 'id_mpio');
	}
}
