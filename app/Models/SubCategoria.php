<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubCategoria
 * 
 * @property int $id_sub_categ
 * @property character varying|null $nom_subcateg
 * @property character varying|null $descripcion
 * @property int|null $id_categ
 * 
 * @property Categoria|null $categoria
 * @property Collection|Reporte[] $reportes
 *
 * @package App\Models
 */
class SubCategoria extends Model
{
	protected $table = 'sub_categorias';
	protected $primaryKey = 'id_sub_categ';
	public $timestamps = false;

	protected $casts = [
		'nom_subcateg' => 'character varying',
		'descripcion' => 'character varying',
		'id_categ' => 'int'
	];

	protected $fillable = [
		'nom_subcateg',
		'descripcion',
		'id_categ'
	];

	public function categoria()
	{
		return $this->belongsTo(Categoria::class, 'id_categ');
	}

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'id_sub_categ');
	}
}
