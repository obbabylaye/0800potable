<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Departamento
 * 
 * @property int $id_depto
 * @property character varying|null $nom_depto
 * @property int $id_acueducto
 * 
 * @property Acueducto $acueducto
 * @property Collection|Municipio[] $municipios
 *
 * @package App\Models
 */
class Departamento extends Model
{
	protected $table = 'departamentos';
	protected $primaryKey = 'id_depto';
	public $timestamps = false;

	protected $casts = [
		'nom_depto' => 'character varying',
		'id_acueducto' => 'int'
	];

	protected $fillable = [
		'nom_depto',
		'id_acueducto'
	];

	public function acueducto()
	{
		return $this->belongsTo(Acueducto::class, 'id_acueducto');
	}

	public function municipios()
	{
		return $this->hasMany(Municipio::class, 'id_depto');
	}
}
