<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Usuario
 * 
 * @property int $id_us
 * @property character varying|null $nom_us
 * @property character varying|null $clave
 * @property bool|null $estatus
 * @property int $id_data_us
 * @property int $id_rol
 * 
 * @property DataU $data_u
 * @property Role $role
 * @property Collection|Reporte[] $reportes
 * @property Collection|HistoricoReport[] $historico_reports
 * @property Collection|ModificacionesReport[] $modificaciones_reports
 * @property Collection|Hreporte[] $hreportes
 *
 * @package App\Models
 */
class Usuario extends Model
{
	protected $table = 'usuarios';
	protected $primaryKey = 'id_us';
	public $timestamps = false;

	protected $casts = [
		'nom_us' => 'character varying',
		'clave' => 'character varying',
		'estatus' => 'bool',
		'id_data_us' => 'int',
		'id_rol' => 'int'
	];

	protected $fillable = [
		'nom_us',
		'clave',
		'estatus',
		'id_data_us',
		'id_rol'
	];

	public function data_u()
	{
		return $this->belongsTo(DataU::class, 'id_data_us');
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'id_rol');
	}

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'id_us');
	}

	public function historico_reports()
	{
		return $this->hasMany(HistoricoReport::class, 'id_us');
	}

	public function modificaciones_reports()
	{
		return $this->hasMany(ModificacionesReport::class, 'id_us');
	}

	public function hreportes()
	{
		return $this->hasMany(Hreporte::class, 'id_us');
	}
}
