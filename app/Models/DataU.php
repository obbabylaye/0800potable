<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DataU
 * 
 * @property int $id_data_us
 * @property character varying|null $nombre
 * @property character varying|null $apellido
 * @property character varying|null $direccion
 * @property int|null $telefono
 * @property character varying|null $correo
 * 
 * @property Collection|Usuario[] $usuarios
 *
 * @package App\Models
 */
class DataU extends Model
{
	protected $table = 'data_us';
	protected $primaryKey = 'id_data_us';
	public $timestamps = false;

	protected $casts = [
		'nombre' => 'character varying',
		'apellido' => 'character varying',
		'direccion' => 'character varying',
		'telefono' => 'int',
		'correo' => 'character varying'
	];

	protected $fillable = [
		'nombre',
		'apellido',
		'direccion',
		'telefono',
		'correo'
	];

	public function usuarios()
	{
		return $this->hasMany(Usuario::class, 'id_data_us');
	}
}
