<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * 
 * @property int $id_rol
 * @property character varying|null $nom_rol
 * 
 * @property Collection|Usuario[] $usuarios
 *
 * @package App\Models
 */
class Role extends Model
{
	protected $table = 'roles';
	protected $primaryKey = 'id_rol';
	public $timestamps = false;

	protected $casts = [
		'nom_rol' => 'character varying'
	];

	protected $fillable = [
		'nom_rol'
	];

	public function usuarios()
	{
		return $this->hasMany(Usuario::class, 'id_rol');
	}
}
